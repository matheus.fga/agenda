import java.util.Scanner;
import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();

    //declarando variável de loop
    boolean continuar = true;
    boolean continuar2 = true;
    String opcaoCadastro;
   

	 while(continuar){ 


	    //interagindo com usuário
	    System.out.println("Digite o nome da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umNome = entradaTeclado;
	    umaPessoa.setNome(umNome);

	    System.out.println("Digite o telefone da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umTelefone = entradaTeclado;
	    umaPessoa.setTelefone(umTelefone);

	    System.out.println("Digite a idade da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umaIdade = entradaTeclado;
	    umaPessoa.setIdade(umaIdade);

	    System.out.println("Quel o sexo da Pessoa M/F:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umSexo = entradaTeclado;
	    umaPessoa.setSexo(umSexo);

	    System.out.println("Digite o email da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umEmail = entradaTeclado;
	    umaPessoa.setEmail(umEmail);

	    System.out.println("Digite o Hangout da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umHangout = entradaTeclado;
	    umaPessoa.setHangout(umHangout);

	    System.out.println("Digite o endereço da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umEndereco = entradaTeclado;
	    umaPessoa.setEndereco(umEndereco);

	    System.out.println("Digite o RG da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umRg = entradaTeclado;
	    umaPessoa.setRg(umRg);

	    System.out.println("Digite o CPF da Pessoa:");
	    entradaTeclado = leitorEntrada.readLine();
	    String umCpf = entradaTeclado;
	    umaPessoa.setCpf(umCpf);

	    

	    //adicionando uma pessoa na lista de pessoas do sistema
	    String mensagem = umControle.adicionar(umaPessoa);

	    //conferindo saída
	    System.out.println("=================================");
	    System.out.println(mensagem);
	    System.out.println("=)\n");
	    
	    //Condição para sair do loop
	    System.out.println("Deseja cadastrar mais uma pessoa s/n:");
	    entradaTeclado = leitorEntrada.readLine();
	    opcaoCadastro =  entradaTeclado;
	    String opacaoNegativa = "n";
	     
	    
	    if( opcaoCadastro.equalsIgnoreCase(opacaoNegativa)){ 
		  continuar=false;
	    }

	}

    //Declarando variável de opção do usuário
    int opcao;
    Scanner entrada = new Scanner(System.in);

 	while(continuar2){ 

	    //Menu de opções
	    System.out.println("\n1 - Pesquisar uma Pessoa");
	    System.out.println("2 - Remover uma Pessoa ");
	    System.out.println("3 - Concluir cadastro ");
	    System.out.println("Entre com a opção desejada [1,2,3]: ");
	    opcao = entrada.nextInt();


	

		switch( opcao )
		{
			case 2:

			    //removendo uma pessoa da lista de pessoas do sistema
			    System.out.println("\nDigite o nome da Pessoa que deseja remover:");
			    entradaTeclado = leitorEntrada.readLine();
			    String nomeRemover = entradaTeclado;
			    Pessoa pessoaRemover = umControle.pesquisar(nomeRemover);
			    String mensagem2 = umControle.remover(pessoaRemover);
			    //conferindo saída
			    System.out.println("=================================");
			    System.out.println(mensagem2);
			    System.out.println("=)");
			    break;
			case 1:

			    //Pesquisando uma pessoa na lista de pessoas do sistema
			    System.out.println("\nDigite o nome da Pessoa que deseja pesquisar:");
			    entradaTeclado = leitorEntrada.readLine();
			    String nomePesquisar = entradaTeclado;
			    Pessoa mensagem3 = umControle.pesquisar(nomePesquisar);

			    //conferindo
			    System.out.println(mensagem3.getNome() + " de " + mensagem3.getIdade() + " anos " + " tem o telefone " + mensagem3.getTelefone() + ",seu email é " + mensagem3.getEmail() + ",seu hangout " + mensagem3.getHangout() + ",mora no endereço " + mensagem3.getEndereco() + ",seu RG é " + mensagem3.getRg() + "e seu CPF " + mensagem3.getCpf() + ".");
			    break;

			default:

			    continuar2=false;
			    System.out.println("Cadastro concluído!!!");

		}
		 
	}
  }

}
